package main

import (
	"gitlab.com/road2antho/backend/internal/pkg/server"
)

func main() {
	srv, err := server.CreateServer(8080)
	if err != nil {
		panic(err)
	}

	// srv.LoadFromCsv("champ.csv")

	srv.ListenAndServe()
}
