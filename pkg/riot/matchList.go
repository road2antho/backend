package riot

import (
	"fmt"
)

// MatchReferenceDto reprensent a past game
type MatchReferenceDto struct {
	PlatformID string `json:"platformId"`
	GameID     int64  `json:"gameId"`
	Champion   int    `json:"champion"`
	Queue      int    `json:"queue"`
	Season     int    `json:"season"`
	Timestamp  int64  `json:"timestamp"`
	Role       string `json:"role"`
	Lane       string `json:"lane"`
}

// MatchlistDto represent the partial list of the players game
type MatchlistDto struct {
	Matches    []MatchReferenceDto `json:"matches"`
	StartIndex int                 `json:"startIndex"`
	EndIndex   int                 `json:"endIndex"`
	TotalGames int                 `json:"totalGames"`
}

// QueryHist retrive the game history of a player
func (c Client) QueryHist(summ SummonerDTO) MatchlistDto {
	query := fmt.Sprintf("/lol/match/v3/matchlists/by-account/%d", summ.AccountID)

	var mList MatchlistDto
	fmt.Println(c.getEndpoint(query, &mList))

	return mList
}
