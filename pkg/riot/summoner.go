package riot

import (
	"fmt"
)

// SummonerDTO represent a summoner account
type SummonerDTO struct {
	ID            int    `json:"id"`
	AccountID     int    `json:"accountId"`
	Name          string `json:"name"`
	ProfileIconID int    `json:"profileIconId"`
	RevisionDate  int64  `json:"revisionDate"`
	SummonerLevel int    `json:"summonerLevel"`
}

// QueryGame return a summoner from it's name
func (c Client) QueryGame(s string) (summoner SummonerDTO) {
	query := fmt.Sprintf("/lol/summoner/v3/summoners/by-name/%s", s)

	fmt.Println(c.getEndpoint(query, &summoner))
	return
}
