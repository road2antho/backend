package riot

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Client is the object used to make call to the RIOT API
type Client struct {
	key string
}

// NewClient create a client from a key
func NewClient(key string) Client {
	println(key)

	return Client{
		key: key,
	}
}

func (c Client) getEndpoint(url string, object interface{}) error {
	query := fmt.Sprintf("https://euw1.api.riotgames.com/%s?api_key=%s", url, c.key)

	res, err := http.Get(query)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	err = json.NewDecoder(res.Body).Decode(object)
	if err != nil {
		return err
	}

	fmt.Printf("%v\n", object)

	return nil
}
