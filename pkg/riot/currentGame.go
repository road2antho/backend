package riot

import (
	"fmt"
)

// CurrentGameInfo represent every information of a live game
type CurrentGameInfo struct {
	GameID       int64  `json:"gameId"`
	MapID        int    `json:"mapId"`
	GameMode     string `json:"gameMode"`
	GameType     string `json:"gameType"`
	Participants []struct {
		TeamID                   int           `json:"teamId"`
		Spell1ID                 int           `json:"spell1Id"`
		Spell2ID                 int           `json:"spell2Id"`
		ChampionID               int           `json:"championId"`
		ProfileIconID            int           `json:"profileIconId"`
		SummonerName             string        `json:"summonerName"`
		Bot                      bool          `json:"bot"`
		SummonerID               int           `json:"summonerId"`
		GameCustomizationObjects []interface{} `json:"gameCustomizationObjects"`
		Perks                    struct {
			PerkIds      []int `json:"perkIds"`
			PerkStyle    int   `json:"perkStyle"`
			PerkSubStyle int   `json:"perkSubStyle"`
		} `json:"perks"`
	} `json:"participants"`
	Observers struct {
		EncryptionKey string `json:"encryptionKey"`
	} `json:"observers"`
	PlatformID      string        `json:"platformId"`
	BannedChampions []interface{} `json:"bannedChampions"`
	GameStartTime   int64         `json:"gameStartTime"`
	GameLength      int           `json:"gameLength"`
}

// QueryCurrentGame queries the live game of a player
func (c Client) QueryCurrentGame(s SummonerDTO) (curr CurrentGameInfo, err error) {
	path := fmt.Sprintf("/lol/spectator/v3/active-games/by-summoner/%d", s.ID)

	err = c.getEndpoint(path, &curr)
	return
}
