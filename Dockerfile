FROM golang:alpine as builder

# Install dep
RUN apk --no-cache add curl gcc git libc-dev protobuf protobuf-dev
RUN curl -fsSL -o /usr/local/bin/dep https://github.com/golang/dep/releases/download/v0.5.0/dep-linux-amd64 && chmod +x /usr/local/bin/dep
RUN go get -u github.com/golang/protobuf/protoc-gen-go

WORKDIR /go/src/gitlab.com/road2antho/backend

# INstall dependencies
COPY Gopkg.toml Gopkg.lock ./
RUN dep ensure -vendor-only


COPY . /go/src/gitlab.com/road2antho/backend
RUN go generate
RUN go build -ldflags="-w -s " ./cmd/backend


# We need libc (musl)
FROM alpine
RUN apk --no-cache add ca-certificates
EXPOSE 8080
COPY --from=builder /go/src/gitlab.com/road2antho/backend/backend /bin/
ENTRYPOINT ["/bin/backend"]
