package server

import (
	"crypto/rand"
	"crypto/sha256"
	// "errors"
	// "strconv"
	"time"

	"gitlab.com/road2antho/backend/internal/pkg/protoGo"

	"encoding/base64"
	"encoding/json"
	"fmt"
	// "io/ioutil"
	"log"
	"net/http"
	"net/url"

	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	// "google.golang.org/api/sheets/v4"
	"github.com/spf13/viper"
	"google.golang.org/api/drive/v3"
	"google.golang.org/grpc/metadata"
)

type AdminServer struct {
	config *oauth2.Config
	redis  *redis.Client
	db     *gorm.DB
}

func NewAdminServer(db *gorm.DB) (*AdminServer, error) {
	viper.AutomaticEnv()
	viper.SetEnvPrefix("backend")

	log.Println("Using redis server at ", viper.GetString("redis_url"))
	client := redis.NewClient(&redis.Options{
		Addr:     viper.GetString("redis_url"),
		Password: "",
	})
	client.WrapProcess(func(old func(cmd redis.Cmder) error) func(cmd redis.Cmder) error {
		return func(cmd redis.Cmder) error {
			fmt.Printf("starting processing: <%s>\n", cmd)
			err := old(cmd)
			fmt.Printf("finished processing: <%s>\n", cmd)
			return err
		}
	})

	b := viper.GetString("json_cred")
	/* b, err := ioutil.ReadFile(credentials)
	if err != nil {
		return nil, err
	} */

	// config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets.readonly")
	config, err := google.ConfigFromJSON([]byte(b), "https://www.googleapis.com/auth/drive.readonly")
	if err != nil {
		return nil, err
	}

	return &AdminServer{
		config,
		client,
		db,
	}, nil
}

func getUserKey(ctx context.Context) (string, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return "", fmt.Errorf("Unable to extract metadata")
	}

	if tks, found := md["token"]; found && len(tks) == 1 {
		return tks[0], nil
	}

	return "", fmt.Errorf("Can't find token from context")
}

func (s AdminServer) CallkackUrl() string {
	url, err := url.Parse(s.config.RedirectURL)
	if err != nil {
		return ""
	}

	return url.Path
}

// ServeHTTP serve the callback url
func (s AdminServer) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	userKey := req.FormValue("state")
	authCode := req.FormValue("code")

	redUrl, err := s.redirectUrlFromRedis(userKey)
	if err != nil {
		redUrl = "https://google.fr"
	}
	u, err := url.Parse(redUrl)
	q := u.Query()

	pkce, err := s.pkceFromRedis(userKey)
	tok, err := s.config.Exchange(
		req.Context(),
		authCode,
		oauth2.SetAuthURLParam("code_verifier", pkce),
	)

	if err != nil {
		log.Printf("Unable to login : %v\n", err)

		q.Set("err", err.Error())
		q.Set("status", "failed")
		u.RawQuery = q.Encode()
		http.Redirect(rep, req, u.String(), http.StatusSeeOther)
		return
	}

	s.tokenToRedis(userKey, tok)

	q.Set("status", "success")
	u.RawQuery = q.Encode()
	http.Redirect(rep, req, u.String(), http.StatusSeeOther)
}

func (s AdminServer) tokenFromRedis(state string) (*oauth2.Token, error) {
	bytes, err := s.redis.Get("oauth:" + state).Bytes()
	if err != nil {
		return nil, err
	}

	var token oauth2.Token
	if err := json.Unmarshal(bytes, &token); err != nil {
		return nil, err
	}

	return &token, nil
}

func (s AdminServer) tokenToRedis(state string, tk *oauth2.Token) error {
	bytes, err := json.Marshal(tk)
	if err != nil {
		return err
	}

	fmt.Println("Set expiry in ", tk.Expiry.Sub(time.Now()), " (It's", time.Now(), " expire at ", tk.Expiry, ")")
	return s.redis.Set("oauth:"+state, bytes, tk.Expiry.Sub(time.Now())).Err()
}

func (s AdminServer) pkceFromRedis(state string) (string, error) {
	return s.redis.Get("pkce:" + state).Result()
}

func (s AdminServer) pkceToRedis(state string, pkce string) error {
	return s.redis.Set("pkce:"+state, pkce, time.Hour).Err()
}

func (s AdminServer) redirectUrlFromRedis(state string) (string, error) {
	return s.redis.Get("redUrl:" + state).Result()
}

func (s AdminServer) redirectUrlToRedis(state string, pkce string) error {
	return s.redis.Set("redUrl:"+state, pkce, time.Hour).Err()
}

func (s *AdminServer) AuthStatus(ctx context.Context, _ *services.Empty) (*services.Status, error) {
	userKey, err := getUserKey(ctx)
	if err != nil {
		return nil, fmt.Errorf("You need to provide a randomly generated token")
	}

	_, err = s.tokenFromRedis(userKey)
	if err != nil {
		return &services.Status{
			AlreadyLogged: false,
		}, nil
	}

	return &services.Status{
		AlreadyLogged: true,
	}, nil
}

func (s *AdminServer) GetOAuthUrl(ctx context.Context, query *services.OauthQuery) (*services.Oauth, error) {
	userKey, err := getUserKey(ctx)
	if err != nil {
		return nil, fmt.Errorf("You need to provide a randomly generated token")
	}

	// Generation of PKCE random string
	randomBytes := make([]byte, 96)
	_, err = rand.Read(randomBytes)
	if err != nil {
		return nil, err
	}
	pkceString := base64.RawURLEncoding.EncodeToString(randomBytes)
	pkceSha := sha256.Sum256([]byte(pkceString))
	pkceEncoded := base64.RawURLEncoding.EncodeToString(pkceSha[:])

	// Store in redis for later use
	err = s.pkceToRedis(userKey, pkceString)
	if err != nil {
		return nil, err
	}

	s.redirectUrlToRedis(userKey, query.GetRedirectionUrl())

	return &services.Oauth{
		Url: s.config.AuthCodeURL(
			userKey,
			oauth2.AccessTypeOnline,
			oauth2.SetAuthURLParam("code_challenge", pkceEncoded),
			oauth2.SetAuthURLParam("code_challenge_method", "S256"),
		),
	}, nil
}

func (s *AdminServer) ResetDatabase(ctx context.Context, req *services.ResetDatabaseQuery) (*services.Empty, error) {
	userKey, err := getUserKey(ctx)
	if err != nil {
		return nil, fmt.Errorf("You need to provide a randomly generated token")
	}

	spreadsheetId := req.SheetId
	if req.ClearBefore {
		// Purge ...
	} else {
		return nil, fmt.Errorf("Unable not to purge")
	}

	tok, err := s.tokenFromRedis(userKey)
	if err != nil {
		return nil, fmt.Errorf("Not authenticated: %v", err)
	}

	client := s.config.Client(ctx, tok)
	driveService, err := drive.New(client)

	rep, err := driveService.Files.Export(spreadsheetId, "text/csv").Download()

	if err != nil {
		return nil, err
	}
	defer rep.Body.Close()

	LoadFromCsv(rep.Body, s.db)

	return &services.Empty{}, nil
}
