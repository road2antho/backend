package server

import (
	// "bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/jinzhu/gorm"

	// Include driver for sqlite databases
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"

	"github.com/improbable-eng/grpc-web/go/grpcweb"

	"gitlab.com/road2antho/backend/internal/pkg/protoGo"
)

// Server represent an HTTP/RPC server with the internal database
type Server struct {
	grpcServer    *grpc.Server
	wrappedServer *grpcweb.WrappedGrpcServer
	httpServer    *http.Server
	db            *gorm.DB

	championSrv *ChampionServer
	adminSrv    *AdminServer
}

// CreateServer create a new server that will bind on the provided port
func CreateServer(port int) (*Server, error) {
	db, err := gorm.Open("sqlite3", "test.db")
	grpclog.Printf("Database errors: %v", err)
	if err != nil {
		return nil, err
	}

	// Set logger
	grpclog.SetLogger(log.New(os.Stdout, "backend: ", log.LstdFlags))

	// Create then register champion server on grpcServer
	grpcServer := grpc.NewServer()
	chSrv := &ChampionServer{db}
	services.RegisterGetChampionsServer(grpcServer, chSrv)
	admSrv, err := NewAdminServer(db)
	if err != nil {
		fmt.Println(err)
	}
	services.RegisterAdminServer(grpcServer, admSrv)

	// Wrap the RPC server to handle WEB request
	wrappedServer := grpcweb.WrapServer(grpcServer, grpcweb.WithWebsockets(true), grpcweb.WithAllowedRequestHeaders([]string{"*"}))

	mux := http.NewServeMux()
	// mux.Handle("/callback", admSrv)
	mux.Handle(admSrv.CallkackUrl(), admSrv)
	/*	mux.HandleFunc("/callback", func(rep http.ResponseWriter, req *http.Request) {
			fmt.Printf("Callback: url %+v\n", req.URL.Query())
			http.Redirect(rep, req, "https://google.com", http.StatusSeeOther)
		})
	*/
	mux.Handle("/", http.FileServer(http.Dir("../frontend/dist")))
	handler := func(resp http.ResponseWriter, req *http.Request) {
		switch {
		case wrappedServer.IsAcceptableGrpcCorsRequest(req) || wrappedServer.IsGrpcWebRequest(req):
			wrappedServer.ServeHTTP(resp, req)

		case wrappedServer.IsGrpcWebSocketRequest(req):
			wrappedServer.HandleGrpcWebsocketRequest(resp, req)

		default:
			// Manual handle
			mux.ServeHTTP(resp, req)
		}
	}

	httpServer := &http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: http.HandlerFunc(handler),
	}

	grpclog.Printf("Starting server. http port: %d", port)

	return &Server{
		grpcServer,
		wrappedServer,
		httpServer,
		db,
		chSrv,
		admSrv,
	}, nil
}

// LoadFromCsv load into the server the champions present in the provided CSV files
func LoadFromCsv(src io.Reader, db *gorm.DB) error {
	db.AutoMigrate(&services.ChampionReply{})
	db.Delete(&services.ChampionReply{})

	reader := csv.NewReader(src)
	reader.Read() // Skip header line
	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		id, err := strconv.Atoi(line[0])
		if err != nil {
			return err
		}

		log.Print("Creating ", line[1], " with id ", id)
		db.Create(&services.ChampionReply{
			Id:           int32(id),
			Name:         line[1],
			A:            line[2],
			Z:            line[3],
			E:            line[4],
			R:            line[5],
			AddData:      line[6],
			Classe:       services.Classe_Dps, //7
			Type:         line[8],
			Degat:        line[9],
			Role:         line[10],
			Role2:        line[11],
			ZoneDeDanger: line[12],
			TempsFort:    line[13],
		})
	}

	return nil
}

// ListenAndServe start the server
func (s Server) ListenAndServe() {
	if err := s.httpServer.ListenAndServe(); err != nil {
		panic(err)
	}
}
