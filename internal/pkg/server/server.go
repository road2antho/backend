package server

import (
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	"golang.org/x/net/context"

	"gitlab.com/road2antho/backend/internal/pkg/protoGo"
	"gitlab.com/road2antho/backend/pkg/riot"
)

// ChampionServer represent an object that can fulfill every request required by the protobuff file
type ChampionServer struct {
	db *gorm.DB
}

// GetChampions returns 10 champions that have a name that contains the word provided
func (s *ChampionServer) GetChampions(chName *services.ChampionName, stream services.GetChampions_GetChampionsServer) error {
	var replyList []services.ChampionReply
	s.db.Limit(10).Find(&replyList, "name LIKE ?", fmt.Sprintf("%%%s%%", chName.GetName()))

	log.Print("### LIST Champion ###")
	for _, r := range replyList {
		log.Print("LIST Champion ", r)
		if err := stream.Send(&services.ChampionId{
			Id:   r.GetId(),
			Name: r.GetName(),
		}); err != nil {
			log.Print("Err ", err)
			return err
		}
	}
	log.Print("")

	return nil
}

// GetChampionInfo returns full information of a champion by ID
func (s *ChampionServer) GetChampionInfo(ctx context.Context, chRq *services.ChampionRequest) (*services.ListChampionReply, error) {
	//s.db.First(reply, &services.ChampionReply{Id: chRq.GetId()})
	var r services.ChampionReply
	s.db.First(&r, chRq.GetId())
	log.Print("Champion ", r)
	return &services.ListChampionReply{
		List: []*services.ChampionReply{&r},
	}, nil
}

// GetPlayersGame returns the list of game by player name
func (s *ChampionServer) GetPlayersGame(player *services.Player, stream services.GetChampions_GetPlayersGameServer) error {
	api := riot.NewClient(os.Getenv("RGAPI"))
	summ := api.QueryGame(player.GetName())

	gameInfo, err := api.QueryCurrentGame(summ)
	if err == nil {
		for _, el := range gameInfo.Participants {
			if el.SummonerName == summ.Name {
				fmt.Println("\t", el)
				var r services.ChampionReply
				s.db.First(&r, el.ChampionID)

				if err := stream.Send(&r); err != nil {
					log.Print("Err ", err)
					return err
				}
			}
		}
	}

	hist := api.QueryHist(summ)
	for _, lastGame := range hist.Matches {
		var r services.ChampionReply
		s.db.First(&r, lastGame.Champion)

		if err := stream.Send(&r); err != nil {
			log.Print("Err ", err)
			return err
		}
	}
	/*
		if len(hist.Matches) != 0 {
			match, err := api.QueryMatch(hist.Matches[0].GameID)
			if err != nil {
				panic(err)
			}
			fmt.Println(match)
			currGame, err := api.QueryCurrentGame(summ)
			if err != nil {
				panic(err)
			}
			fmt.Println(currGame)
		}
	*/
	return nil
}
